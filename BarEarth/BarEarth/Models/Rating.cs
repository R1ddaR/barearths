﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace BarEarth.Models
{
    [Authorize]
    public class Rating
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(10)]
        public int Value { get; set; }
        [StringLength(150)]
        public string Review { get; set; }
        [ForeignKey("BarId")]
        public int BarId { get; set; }
        public Bar Bar { get; set; }
        
    }
}
